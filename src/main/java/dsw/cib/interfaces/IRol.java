package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.RolDTO;

public interface IRol {
	
	public ArrayList<RolDTO>listarRoles();

}
