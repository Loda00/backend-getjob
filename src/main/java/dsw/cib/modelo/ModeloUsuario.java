package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dsw.cib.dto.UsuarioDTO;
import dsw.cib.interfaces.IUsuario;
import dsw.cib.utils.MySQLConnection;

public class ModeloUsuario implements IUsuario{
	
	public int crearUsuarioRapido(UsuarioDTO u) {
		int resultado = -1;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			con = MySQLConnection.getConexion();
			String query = "{CALL crearUsuarioRapido(?, ?, ?, ?, ?)}";
			pst = con.prepareStatement(query);
			pst.setString(1, u.getContrasenia());
			pst.setString(2, u.getNombres());
			pst.setString(3, u.getApellidos());
			pst.setString(4, u.getCorreo());
			pst.setInt(5, u.getIdRol());
			resultado = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en la sentencia "+e.getMessage());
		}finally {
			try {
				if(con!=null)con.close();
				if(pst!=null)pst.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar la conecci�n");
			}
		}
		
		return resultado;
	}
	
	public int crearUsuario(UsuarioDTO u) {
		int resultado = -1;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			con = MySQLConnection.getConexion();
			String query = "{CALL crearUsuario(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			pst = con.prepareStatement(query);
			pst.setString(1, u.getContrasenia());
			pst.setString(2, u.getNombres());
			pst.setString(3, u.getApellidos());
			pst.setString(4, u.getCorreo());
			pst.setString(5, u.getNumero());
			pst.setInt(6, u.getIdEstado());
			pst.setInt(7, u.getIdPais());
			pst.setInt(8, u.getIdDepartamento());
			pst.setInt(9, u.getIdProvincia());
			pst.setInt(10, u.getIdDistrito());
			pst.setInt(11, u.getIdCentroEstudios());
			pst.setInt(12, u.getIdRol());
			pst.setString(13, u.getPresentacion());
			resultado = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en la sentencia "+e.getMessage());
		}finally {
			try {
				if(con!=null)con.close();
				if(pst!=null)pst.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar la conecci�n");
			}
		}
		
		return resultado;
	}

	public ArrayList<UsuarioDTO> listarUsuarios() {
		ArrayList<UsuarioDTO>usuarios= new ArrayList<UsuarioDTO>();
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con=MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_USUARIO";
			pst=con.prepareStatement(sql);
			rs=pst.executeQuery();
			while(rs.next()) {
				UsuarioDTO u = new UsuarioDTO();

				u.setId(rs.getInt(1));
				u.setContrasenia(rs.getString(2));
				u.setNombres(rs.getString(3));
				u.setApellidos(rs.getString(4));
				u.setCorreo(rs.getString(5));
				u.setNumero(rs.getString(6));
				u.setIdEstado(rs.getInt(9));
				u.setIdPais(rs.getInt(10));
				u.setIdDepartamento(rs.getInt(11));
				u.setIdProvincia(rs.getInt(12));
				u.setIdDistrito(rs.getInt(13));
				u.setIdCentroEstudios(rs.getInt(14));
				u.setIdRol(rs.getInt(15));
				u.setPresentacion(rs.getString(16));
				usuarios.add(u);
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return usuarios;	
	}
	
	public int actualizarUsuario(UsuarioDTO u) {
		int resultado = -1;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			System.out.println("Entro Al modelo");
			con = MySQLConnection.getConexion();
			String query = "{CALL sp_actualizarUsuario(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			pst = con.prepareStatement(query);
			pst.setString(1, u.getContrasenia());
			pst.setString(2, u.getNombres());
			pst.setString(3, u.getApellidos());
			pst.setString(4, u.getCorreo());
			pst.setString(5, u.getNumero());
			pst.setInt(6, u.getIdEstado());
			pst.setInt(7, u.getIdPais());
			pst.setInt(8, u.getIdDepartamento());
			pst.setInt(9, u.getIdProvincia());
			pst.setInt(10, u.getIdDistrito());
			pst.setInt(11, u.getIdCentroEstudios());
			pst.setInt(12, u.getIdRol());
			pst.setString(13, u.getPresentacion());
			pst.setInt(14, u.getId());

			resultado = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en la sentencia "+e.getMessage());
		}finally {
			try {
				if(con!=null)con.close();
				if(pst!=null)pst.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar la conexion");
			}
		}	
		return resultado;
	}

	public UsuarioDTO login(String correo, String password) {
		UsuarioDTO u = null;
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con=MySQLConnection.getConexion();
			String sql ="{CALL sp_Login(?,?)}";
			pst=con.prepareStatement(sql);
			pst.setString(1, correo);
			pst.setString(2, password);
			rs=pst.executeQuery();
			while(rs.next()) {
				u = new UsuarioDTO();
				u.setId(rs.getInt(1));
				u.setContrasenia(rs.getString(2));
				u.setNombres(rs.getString(3));
				u.setApellidos(rs.getString(4));
				u.setCorreo(rs.getString(5));
				u.setNumero(rs.getString(6));
				u.setIdEstado(rs.getInt(9));
				u.setIdPais(rs.getInt(10));
				u.setIdDepartamento(rs.getInt(11));
				u.setIdProvincia(rs.getInt(12));
				u.setIdDistrito(rs.getInt(13));
				u.setIdCentroEstudios(rs.getInt(14));
				u.setIdRol(rs.getInt(15));
				u.setPresentacion(rs.getString(16));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		return u;
	}

}
