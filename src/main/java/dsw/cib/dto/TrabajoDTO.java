package dsw.cib.dto;

import java.io.Serializable;

public class TrabajoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String titulo;
	private String descripcion;
	private String otros;
	private int idEstado;
	private int idUsuario;
	private String requisitos;
	private String beneficios;
	private int idArea;
	private String fecha;
	private boolean postulado;
	private String nombres;

	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public boolean isPostulado() {
		return postulado;
	}
	public void setPostulado(boolean postulado) {
		this.postulado = postulado;
	}
	public int getIdArea() {
		return idArea;
	}
	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getRequisitos() {
		return requisitos;
	}
	public void setRequisito(String requisito) {
		requisitos = requisito;
	}
	public String getBeneficios() {
		return beneficios;
	}
	public void setBeneficio(String beneficio) {
		beneficios = beneficio;
	}

	
	
}
