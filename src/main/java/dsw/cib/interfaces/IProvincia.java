package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.ProvinciaDTO;

public interface IProvincia {

	public ArrayList<ProvinciaDTO> listarProvincia();
	
}
