package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.CentroDeEstudiosDTO;
import dsw.cib.interfaces.ICentroDeEstudio;
import dsw.cib.utils.MySQLConnection;

public class ModeloCentroDeEstudios implements ICentroDeEstudio {

	public ArrayList<CentroDeEstudiosDTO> listarCentroDeEstudios() {
		ArrayList<CentroDeEstudiosDTO>listaCentroDeEstudios= new ArrayList<CentroDeEstudiosDTO>();
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con=MySQLConnection.getConexion();
			String sql = "select * from TB_CENTRO_DE_ESTUDIOS";
			pst=con.prepareStatement(sql);
			rs=pst.executeQuery();
			while(rs.next()) {
				CentroDeEstudiosDTO ce = new CentroDeEstudiosDTO();
				ce.setIdCentroE(rs.getInt(1));
				ce.setNombreCentroE(rs.getString(2));				
				listaCentroDeEstudios.add(ce);
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return listaCentroDeEstudios;	
	}

}
