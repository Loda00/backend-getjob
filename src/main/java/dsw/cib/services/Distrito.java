package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.DistritoDTO;
import dsw.cib.modelo.ModeloDistrito;


@Path("/distrito")
public class Distrito {
	
	//http://localhost:8080/Get_Job/pais/listarPaises

	@GET
	@Path("/listarDistritos")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<DistritoDTO> listarDistritos() {
		
		ModeloDistrito modeloDistrito = new ModeloDistrito();
		ArrayList<DistritoDTO> distritos =  modeloDistrito.listarDistritos();

		return distritos;
	}

}
