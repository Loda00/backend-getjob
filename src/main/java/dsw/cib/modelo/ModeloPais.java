package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.PaisDTO;
import dsw.cib.interfaces.IPais;
import dsw.cib.utils.MySQLConnection;

public class ModeloPais implements IPais{

	public ArrayList<PaisDTO> listarPaises(){
		ArrayList<PaisDTO> paises = new ArrayList<PaisDTO>();
		
		ResultSet rs = null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_PAIS;";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				PaisDTO p = new PaisDTO();
				p.setId(rs.getInt(1));
				p.setPais(rs.getString(2));
				paises.add(p);
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return paises;
	}
	
}
