package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dsw.cib.dto.PostulacionDTO;
import dsw.cib.dto.TrabajoDTO;
import dsw.cib.interfaces.ITrabajo;
import dsw.cib.utils.MySQLConnection;

public class ModeloTrabajo implements ITrabajo{

	public ArrayList<TrabajoDTO> listarTrabajos() {
		ArrayList<TrabajoDTO>trabajo= new ArrayList<TrabajoDTO>();
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con=MySQLConnection.getConexion();
			String sql = "{CALL  sp_listarTrabajos()}";
			pst=con.prepareStatement(sql);
			rs=pst.executeQuery();
			while(rs.next()) {
				TrabajoDTO t = new TrabajoDTO();

				t.setId(rs.getInt(1));
				t.setTitulo(rs.getString(2));
				t.setDescripcion(rs.getString(3));
				t.setOtros(rs.getString(4));
				t.setIdEstado(rs.getInt(5));
				t.setIdUsuario(rs.getInt(6));
				t.setRequisito(rs.getString(7));
				t.setBeneficio(rs.getString(8));
				t.setIdArea(rs.getInt(9));
				t.setFecha(rs.getString(10));
				t.setNombres(rs.getString(11));
				trabajo.add(t);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return trabajo;
	}

	public ArrayList<TrabajoDTO> listarTrabajosPorTitulo(String titulo) {
		ArrayList<TrabajoDTO>lista= new ArrayList<TrabajoDTO>();
		ResultSet rs = null; 
		Connection con = null;
		PreparedStatement pst = null;
		try {
		   con = MySQLConnection.getConexion();
		   String sql = "select * from tb_trabajos where titulo = ?";
		   pst = con.prepareStatement(sql);   
		   pst.setString(1,titulo); 		   		   
		   rs = pst.executeQuery();		   
		   while (rs.next()){
			   TrabajoDTO t = new TrabajoDTO();
				t.setId(rs.getInt(1));
				t.setTitulo(rs.getString(2));
				t.setDescripcion(rs.getString(3));
				t.setOtros(rs.getString(4));
				t.setIdEstado(rs.getInt(5));
				t.setIdUsuario(rs.getInt(6));
				t.setRequisito(rs.getString(7));
				t.setBeneficio(rs.getString(8));
				t.setIdArea(rs.getInt(9));
				lista.add(t);
		   }

		} catch (Exception e) {
		   System.out.println("Error en la sentencia ");
		} finally {
			try {
				if(pst!=null) pst.close();
				if(con!=null) con.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar ");
			}
	}  return lista;	
	}

	public ArrayList<TrabajoDTO> listarTrabajosPorCategoria() {
		// TODO Auto-generated method stub
		return null;
	}

	public int crearTrabajo(TrabajoDTO trabajo) {
		int n = -1;
		Connection con = null;
		PreparedStatement pst = null;
		
		try {
			con = MySQLConnection.getConexion();
			String sql = "{call sp_crearTrabajo(?,?,?,?,?,?,?)}";
			pst = con.prepareStatement(sql);
			pst.setString(1, trabajo.getTitulo());
			pst.setString(2, trabajo.getDescripcion());
			pst.setString(3, trabajo.getOtros());
			pst.setInt(4, trabajo.getIdUsuario());
			pst.setString(5, trabajo.getBeneficios());
			pst.setString(6, trabajo.getRequisitos());
			pst.setInt(7, trabajo.getIdArea());
			n = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en la sentencia �" + e);
		} finally {
			try {
				if (pst != null)
					pst.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar ");
			}
		}
		
		
		return n;
	}

	public int postularAlTrabajo(PostulacionDTO p) {
		int resultado = -1;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			con = MySQLConnection.getConexion();
			String query = "{CALL sp_postularAlTrabajo(?, ?)}";
			pst = con.prepareStatement(query);
			pst.setInt(1, p.getIdUsuario());
			pst.setInt(2, p.getIdTrabajo());

			resultado = pst.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en la sentencia "+e.getMessage());
		}finally {
			try {
				if(con!=null)con.close();
				if(pst!=null)pst.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar la conexion");
			}
		}	
		return resultado;
	}

	public int actualizarTrabajo() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int eliminarTrabajo(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
