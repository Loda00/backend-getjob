package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.DepartamentoDTO;
import dsw.cib.interfaces.IDepartamento;
import dsw.cib.utils.MySQLConnection;

public class ModeloDepartamento implements IDepartamento{

	public ArrayList<DepartamentoDTO> listarDepartamentos(){
		
		ArrayList<DepartamentoDTO> departamentos = new ArrayList<DepartamentoDTO>();
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_DEPARTAMENTO";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				DepartamentoDTO d = new DepartamentoDTO();
				d.setId(rs.getInt(1));
				d.setUbigeo(rs.getString(2));
				d.setDepartamento(rs.getString(3));
				d.setIdPais(rs.getInt(4));
				departamentos.add(d);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		return departamentos;
	}
	
}
