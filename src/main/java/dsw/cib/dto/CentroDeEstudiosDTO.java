package dsw.cib.dto;

import java.io.Serializable;

public class CentroDeEstudiosDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int  idCentroE;
	private String nombreCentroE;
	
	
	
	public int getIdCentroE() {
		return idCentroE;
	}
	public void setIdCentroE(int idCentroE) {
		this.idCentroE = idCentroE;
	}
	public String getNombreCentroE() {
		return nombreCentroE;
	}
	public void setNombreCentroE(String nombreCentroE) {
		this.nombreCentroE = nombreCentroE;
	}
	
	
	
	
}
