package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.DetalleTrabajoDTO;
import dsw.cib.interfaces.IDetalleTrabajo;
import dsw.cib.utils.MySQLConnection;

public class ModeloDetalleTrabajo implements IDetalleTrabajo{

	public ArrayList<DetalleTrabajoDTO> listarDetalleTrabajos(int id) {
		ArrayList<DetalleTrabajoDTO> trabajos = new ArrayList<DetalleTrabajoDTO>();

		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con=MySQLConnection.getConexion();
			String sql = "{CALL  sp_listarTrabajosPostulados(?)}";
			   pst = con.prepareStatement(sql);   
			   pst.setInt(1, id); 		   		   
			   rs = pst.executeQuery();	
			while(rs.next()) {
				DetalleTrabajoDTO dt = new DetalleTrabajoDTO();
				
				dt.setIdUsuario(rs.getInt(1));
				dt.setIdTrabajo(rs.getInt(2));
				dt.setIdEstado(rs.getInt(3));
				dt.setCorreo(rs.getString(4));
				dt.setNumero(rs.getString(5));
				dt.setIdRol(rs.getInt(6));
				dt.setTitulo(rs.getString(7));
				dt.setDescripcion(rs.getString(8));
				dt.setOtros(rs.getString(9));
				dt.setRequisitos(rs.getString(10));
				dt.setBeneficios(rs.getString(11));
				dt.setIdArea(rs.getInt(12));
				trabajos.add(dt);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}

		return trabajos;
	}
	
}
