package dsw.cib.modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import dsw.cib.interfaces.ICorreo;
import dsw.cib.utils.MySQLConnection;
public class ModeloCorreo implements ICorreo{

	public boolean existeCorreo(String correo) {
		
		boolean resultado = false;
		Connection con = null;
		CallableStatement pst = null;
		ResultSet rs = null;
		try {
			con = MySQLConnection.getConexion();
			String query = "{CALL existeCorreo(?)}";
			pst = con.prepareCall(query);
			pst.setString(1, correo);
			rs = pst.executeQuery();
			while(rs.next()){
				System.out.println("CORREO EXISTE");
				resultado = true;
			}
		} catch (Exception e) {
			System.out.println("Error en la sentencia "+e.getMessage());
		}finally {
			try {
				if(con!=null)con.close();
				if(pst!=null)pst.close();
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				System.out.println("Error al cerrar la conecci�n");
			}
		}
		
		return  resultado;
	}
	
}
