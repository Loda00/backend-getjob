package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dsw.cib.dto.PaisDTO;
import dsw.cib.dto.ResponseDTO;
import dsw.cib.modelo.ModeloPais;

@Path("/pais")
public class Pais {

	//http://localhost:8080/Get_Job/pais/listarPaises

	@GET
	@Path("/listarPaises")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<PaisDTO> listarPaises() {
		
		ModeloPais modeloPais = new ModeloPais();
		ArrayList<PaisDTO> paises =  modeloPais.listarPaises();

		return paises;
	}
	
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	public Response prueba() {

		ResponseDTO rs = new ResponseDTO();

		rs.setError("Tienes un error");
		
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rs).build();
	}
}
