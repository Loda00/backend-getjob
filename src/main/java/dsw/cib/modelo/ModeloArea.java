package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.AreaDTO;
import dsw.cib.interfaces.IArea;
import dsw.cib.utils.MySQLConnection;

public class ModeloArea implements IArea{

	public ArrayList<AreaDTO> listaAreas() {
		ArrayList<AreaDTO> areas = new ArrayList<AreaDTO>();
		
		ResultSet rs = null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_AREA;";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				AreaDTO a = new AreaDTO();
				a.setId(rs.getInt(1));
				a.setArea(rs.getString(2));
				areas.add(a);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		return areas;
	}
	
}
