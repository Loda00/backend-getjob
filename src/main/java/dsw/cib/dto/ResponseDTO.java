package dsw.cib.dto;

public class ResponseDTO {

	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
