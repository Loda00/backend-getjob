package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.DepartamentoDTO;
import dsw.cib.modelo.ModeloDepartamento;

@Path("/departamento")
public class Departamento{

	@GET
	@Path("/listarDepartamentos")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<DepartamentoDTO> listarDepartamento() {

		ModeloDepartamento modeloDepartamento = new ModeloDepartamento();
		ArrayList<DepartamentoDTO> departamento = modeloDepartamento.listarDepartamentos();
		
		return departamento;
	}
	
}
