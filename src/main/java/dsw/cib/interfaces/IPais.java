package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.PaisDTO;

public interface IPais {

	public ArrayList<PaisDTO> listarPaises();

}
