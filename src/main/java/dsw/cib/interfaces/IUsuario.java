package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.UsuarioDTO;

public interface IUsuario {
	
	public int crearUsuario(UsuarioDTO u);
	public ArrayList<UsuarioDTO> listarUsuarios();
	

}
