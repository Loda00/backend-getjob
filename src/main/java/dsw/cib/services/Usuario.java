package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dsw.cib.dto.ResponseDTO;
import dsw.cib.dto.UsuarioDTO;
import dsw.cib.modelo.ModeloCorreo;
import dsw.cib.modelo.ModeloUsuario;


@Path("/usuario")
public class Usuario{

	@POST
	@Path("/crearUsuario")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crearUsuario(UsuarioDTO u) {

		boolean existeCorreo = correoYaRegistrado(u.getCorreo());

		if (existeCorreo == false) {

			ModeloUsuario mu = new ModeloUsuario();
			int resultado = mu.crearUsuario(u);

			if (resultado != -1) {
				return Response.status(Response.Status.OK)
						.entity(u)
						.type(MediaType.APPLICATION_JSON)
						.build();
			}
		}

		return Response.status(Response.Status.BAD_REQUEST)
				.entity("Correo ya fue registrado")
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	@POST
	@Path("/crearUsuarioRapido")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crearUsuarioRapido(UsuarioDTO u) {

		boolean existeCorreo = correoYaRegistrado(u.getCorreo());
		if (existeCorreo == false) {

			ModeloUsuario mu = new ModeloUsuario();
			int resultado = mu.crearUsuarioRapido(u);
			System.out.println("resultado => " + resultado);
			if (resultado != -1) {
				return Response.status(Response.Status.OK)
						.entity(u)
						.type(MediaType.APPLICATION_JSON)
						.build();
			}
		}

		return Response.status(Response.Status.BAD_REQUEST)
				.entity("Correo ya fue registrado")
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	public boolean correoYaRegistrado(String correo) {
		
		ModeloCorreo mc = new ModeloCorreo();
		
		boolean existe = mc.existeCorreo(correo);

		if (existe == true) {
			return true;
		}
		return false;
	}
	 //http://localhost:8080/Get_Job/usuario/listarUsuarios
	
	@GET
	@Path("/listarUsuarios")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<UsuarioDTO> listarUsuarios() {
		ModeloUsuario modeloUsuario = new ModeloUsuario();
		ArrayList<UsuarioDTO> usuario = modeloUsuario.listarUsuarios();		
		return usuario;		
	}

	@POST
	@Path("/loginUsuario")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON})
	public Response loginUsuario(UsuarioDTO u) {
		System.out.println("USWERRRRRR" + u);
		System.out.println("correo " + u.getCorreo() + "password" + u.getContrasenia());
		ModeloUsuario modeloUsuario = new ModeloUsuario();
		UsuarioDTO usuario = modeloUsuario.login(u.getCorreo(), u.getContrasenia());

		if (usuario == null) {

			ResponseDTO response = new ResponseDTO();

			response.setError("Usuario o contrasenia incorrectos");

			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(response)
					.type(MediaType.APPLICATION_JSON)
					.build();			
		}

		return Response
				.status(Response.Status.OK)
				.entity(usuario)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}

}
