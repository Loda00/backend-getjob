package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.RolDTO;
import dsw.cib.interfaces.IRol;
import dsw.cib.utils.MySQLConnection;

public class ModeloRol implements IRol{
	
	public ArrayList<RolDTO>listarRoles(){
		ArrayList<RolDTO> rol = new ArrayList<RolDTO>();
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_ROL;";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				RolDTO r = new RolDTO();
				r.setId(rs.getInt(1));
				r.setRol(rs.getString(2));
				rol.add(r);
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return rol;
	}
}
