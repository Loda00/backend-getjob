package dsw.cib.services;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.sun.jersey.multipart.FormDataParam;

@Path("/aws")
public class PruebaS3 {

	AWSCredentials credenciales = new BasicAWSCredentials("AKIAQB2C5VJFPPRNWUNU", "Sm2SNvprcAdZw7fv0+HKoqH0D3MYBQbNjcuF10XR");
	AmazonS3 s3 = AmazonS3ClientBuilder.standard()
	        .withCredentials(new AWSStaticCredentialsProvider(credenciales))
	        .withRegion(Regions.US_EAST_2)
	        .build();

	@POST
	@Path("/uploadFile")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
	public List<Bucket> uploadFile(@FormDataParam("file")InputStream inputStreamFile,
								   @FormDataParam("file")String metaDataFile) {
		
		// metaDataFile.getFileName();
		// System.out.println("upInputStreamupInputStream " + inputStreamFile + " -----  " + metaDataFile);
		List<Bucket> buckets = null;
		
		File file = new File(metaDataFile);
			
		String bucket_name = "get-job";
		String key_name = "test.jpg";
		System.out.println("iniciando");
		
		System.out.println("leyendo la imagen");
		try {
			s3.putObject(bucket_name, key_name, file);
		} catch (AmazonS3Exception e) {
			System.out.println(e.getErrorMessage());
		}

		return buckets;
	}

	@GET
	@Path("/listBuckets")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Bucket> listBuckets() {
		List<Bucket> buckets = null;
		try {
			buckets = s3.listBuckets();
		} catch (AmazonS3Exception e) {
			System.out.println(e.getErrorMessage());
		}

		return buckets;
	}
	
	
	@POST
	@Path("/createBucket")
	@Produces(MediaType.TEXT_PLAIN)
	public String createBucket() {
		
		//final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

		System.out.println("credencialess3" + s3);
		String nameBucket = "bucket-de-prueba-para-avance-dws";
		
		try {
			s3.createBucket(nameBucket);
		} catch (AmazonS3Exception e) {
			System.out.println(e.getErrorMessage());
		}
		
		return "Bucket Creado !";
	}
	
}
