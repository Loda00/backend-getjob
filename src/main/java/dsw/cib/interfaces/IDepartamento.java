package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.DepartamentoDTO;

public interface IDepartamento {

	public ArrayList<DepartamentoDTO> listarDepartamentos();
	
}
