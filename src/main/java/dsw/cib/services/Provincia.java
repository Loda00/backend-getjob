package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.ProvinciaDTO;
import dsw.cib.modelo.ModeloProvincia;

@Path("/provincia")
public class Provincia {
	
	@GET
	@Path("/listarProvincias")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ProvinciaDTO> listarProvincia() {

		ModeloProvincia mp = new ModeloProvincia();
		ArrayList<ProvinciaDTO> provincias = mp.listarProvincia();
		
		return provincias;
	}
}
