package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.DistritoDTO;

public interface IDistrito {
	
	public ArrayList<DistritoDTO> listarDistritos();

}
