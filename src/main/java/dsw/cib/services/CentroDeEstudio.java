package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.CentroDeEstudiosDTO;
import dsw.cib.modelo.ModeloCentroDeEstudios;

@Path("/centroDeEstudio")
public class CentroDeEstudio {

	//http://localhost:8080/Get_Job/centroDeEstudio/listarCentroDeEstudio
	@GET
	@Path("/listarCentroDeEstudios")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<CentroDeEstudiosDTO> listaCentroDeEstudios() 
	{
		ModeloCentroDeEstudios modeloCentro = new ModeloCentroDeEstudios();
		ArrayList<CentroDeEstudiosDTO> centroDeEstudios = modeloCentro.listarCentroDeEstudios();
		return centroDeEstudios;
	}
	
}
