package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.ProvinciaDTO;
import dsw.cib.interfaces.IProvincia;
import dsw.cib.utils.MySQLConnection;

public class ModeloProvincia implements IProvincia{

	public ArrayList<ProvinciaDTO> listarProvincia() {
		
		ArrayList<ProvinciaDTO> provincia = new ArrayList<ProvinciaDTO>();
		
		ResultSet rs= null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_PROVINCIA";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				ProvinciaDTO p = new ProvinciaDTO();
				p.setId(rs.getInt(1));
				p.setUbigeo(rs.getString(2));
				p.setProvincia(rs.getString(3));
				p.setIdDepartamento(rs.getInt(4));
				provincia.add(p);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return provincia;
	}
	
}
