package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dsw.cib.dto.DetalleTrabajoDTO;
import dsw.cib.dto.PostulacionDTO;
import dsw.cib.dto.ResponseDTO;
import dsw.cib.dto.TrabajoDTO;
import dsw.cib.dto.UsuarioDTO;
import dsw.cib.modelo.ModeloDetalleTrabajo;
import dsw.cib.modelo.ModeloTrabajo;


@Path("/trabajo")
public class Trabajo {
	
	//http://localhost:8080/Get_Job/trabajo/crearTrabajo

	@POST
	@Path("/crearTrabajo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response trabajo (TrabajoDTO dto) {
		ModeloTrabajo modeloTrabajo = new ModeloTrabajo();
		int response = modeloTrabajo.crearTrabajo(dto);
		System.out.println("response" + response);
		if (response != -1) {
			return Response.status(Response.Status.OK)
			.entity(dto)
			.type(MediaType.APPLICATION_JSON)
			.build();
		}
		
		ResponseDTO rs = new ResponseDTO();
		rs.setError("Ocurrió un error al crear el trabajo");
		
		return Response.status(Response.Status.BAD_REQUEST)
				.entity(rs)
				.type(MediaType.APPLICATION_JSON)
				.build();				
	}
	
	@POST
	@Path("/postularAlTrabajo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postularAlTrabajo (PostulacionDTO p) {
		ModeloTrabajo modeloTrabajo = new ModeloTrabajo();
		int response = modeloTrabajo.postularAlTrabajo(p);
		
		if (response != -1) {
			return Response.status(Response.Status.OK)
					.entity(p)
					.type(MediaType.APPLICATION_JSON)
					.build();
		}
		return Response.status(Response.Status.BAD_REQUEST)
				.entity("Ocurrió un error al postular")
				.type(MediaType.APPLICATION_JSON)
				.build();		
	}

	//http://localhost:8080/Get_Job/trabajo/listarTrabajos
	//http://localhost:8080/Get_Job/trabajo/listarTrabajos
	@GET
	@Path("/listarTrabajos/{id_usuario}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarTrabajos(@PathParam("id_usuario")int id) {
		System.out.println("IDIDIDIDI" + id);
		ModeloTrabajo modeloTrabajo = new ModeloTrabajo();
		ArrayList<TrabajoDTO> trabajo = modeloTrabajo.listarTrabajos();
		
		ModeloDetalleTrabajo modeloDetalleTrabajo = new ModeloDetalleTrabajo();
		ArrayList<DetalleTrabajoDTO> detalleTrabajo =  modeloDetalleTrabajo.listarDetalleTrabajos(id);
		
		for (TrabajoDTO t : trabajo) {
			for (DetalleTrabajoDTO dt : detalleTrabajo) {
				if (t.getId() == dt.getIdTrabajo()) {
					t.setPostulado(true);
				}
			}
		}

		return Response.status(Response.Status.OK)
				.entity(trabajo)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	
	
	//http://localhost:8080/Get_Job/trabajo/listarTrabajosTitulos/desarrollador web
	@GET
	@Path("/listarTrabajosTitulos/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<TrabajoDTO> listarTrabajosTitulos(@PathParam("p_titulo")String ti) {	
		ModeloTrabajo modeloTrabajo = new ModeloTrabajo();
		ArrayList<TrabajoDTO> trabajo = modeloTrabajo.listarTrabajosPorTitulo(ti);		
		return trabajo;
	}
	

}
