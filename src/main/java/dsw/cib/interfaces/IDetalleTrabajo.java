package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.DetalleTrabajoDTO;
import dsw.cib.dto.UsuarioDTO;

public interface IDetalleTrabajo {

	public ArrayList<DetalleTrabajoDTO> listarDetalleTrabajos(int id);
	
}
