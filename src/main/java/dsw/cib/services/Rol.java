package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.RolDTO;
import dsw.cib.modelo.ModeloRol;

@Path("/rol")
public class Rol {
	
	//http://localhost:8080/Get_Job/rol/listarRoles

	@GET
	@Path("/listarRoles")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<RolDTO> prueba() {
		
		ModeloRol r = new ModeloRol();
		ArrayList<RolDTO> roles =  r.listarRoles();

		return roles;
	}
}
