package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.AreaDTO;

public interface IArea {
	
	public ArrayList<AreaDTO> listaAreas();
	
}
