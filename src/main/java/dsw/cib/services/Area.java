package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.AreaDTO;
import dsw.cib.modelo.ModeloArea;

@Path("/area")
public class Area {

	@GET
	@Path("/listarAreas")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<AreaDTO> listarAreas() {
		ModeloArea modeloCentro = new ModeloArea();
		ArrayList<AreaDTO> areas = modeloCentro.listaAreas();
		return areas;
	}
}
