package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.CentroDeEstudiosDTO;

public interface ICentroDeEstudio {
	public ArrayList<CentroDeEstudiosDTO> listarCentroDeEstudios();
}
