package dsw.cib.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dsw.cib.dto.DistritoDTO;
import dsw.cib.interfaces.IDistrito;
import dsw.cib.utils.MySQLConnection;

public class ModeloDistrito implements IDistrito{

	public ArrayList<DistritoDTO> listarDistritos() {
		ArrayList<DistritoDTO> distritos = new ArrayList<DistritoDTO>();

		ResultSet rs = null;
		Connection con =null;
		PreparedStatement pst=null;
		try {
			con = MySQLConnection.getConexion();
			String sql = "SELECT * FROM TB_DISTRITO;";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()) {
				DistritoDTO d = new DistritoDTO();
				d.setId(rs.getInt(1));
				d.setDistrito(rs.getString(2));
				d.setUbigeo(rs.getString(3));
				d.setIdProvincia(rs.getInt(4));
				distritos.add(d);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
			}
		}
		
		return distritos;
	}
	
}
