package dsw.cib.interfaces;

import java.util.ArrayList;

import dsw.cib.dto.PostulacionDTO;
import dsw.cib.dto.TrabajoDTO;

public interface ITrabajo {

	public ArrayList<TrabajoDTO> listarTrabajos();
	
	public ArrayList<TrabajoDTO> listarTrabajosPorTitulo(String titulo);
	
	public ArrayList<TrabajoDTO> listarTrabajosPorCategoria();
	
	public int crearTrabajo(TrabajoDTO trabajo);
	
	public int actualizarTrabajo();
	
	public int eliminarTrabajo(int id);
	
	public int postularAlTrabajo(PostulacionDTO p);

}
