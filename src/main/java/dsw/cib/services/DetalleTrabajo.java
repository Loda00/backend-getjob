package dsw.cib.services;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dsw.cib.dto.DetalleTrabajoDTO;
import dsw.cib.dto.UsuarioDTO;
import dsw.cib.modelo.ModeloDetalleTrabajo;

@Path("/detalleTrabajo")
public class DetalleTrabajo {

	@POST
	@Path("/listarDetalleTrabajos")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<DetalleTrabajoDTO> listarDetalleTrabajos(UsuarioDTO u) {
		System.out.println("IDIDIDID" + u.getId());
		ModeloDetalleTrabajo detalleTrabajo = new ModeloDetalleTrabajo();
		ArrayList<DetalleTrabajoDTO> areas = detalleTrabajo.listarDetalleTrabajos(u.getId());
		return areas;
	}
}
