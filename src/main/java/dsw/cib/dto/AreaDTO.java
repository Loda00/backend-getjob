package dsw.cib.dto;

import java.io.Serializable;

public class AreaDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String area;

	public int getId() {
		return id;
	}
	public void setId(int idArea) {
		this.id = idArea;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
}
